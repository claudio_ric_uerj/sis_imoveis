$(document).ready(function(){

    $('.del-imv').each(function(){
        $(this).on('click',function(){
            var form = $(this).children('form');
            bootbox.confirm({
                message: "Você realmente deseja deletar esse registro?",
                buttons: {
                    confirm: {
                        label: 'Sim',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'Não',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if (result){
                        form.submit();
                    }
                }
            });
        });

        
    })


    $('.edit-imv').each(function(){
        $(this).on('click',function(){
            var form = $(this).children('form');
            form.submit();
        });
    })
    
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).children('img').css('display','block');

        $(this).ekkoLightbox();
    });

});