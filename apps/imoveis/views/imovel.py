# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from apps.imoveis.models import Imovel
from apps.imoveis.forms import SearchForm, CadastroForm



# Create your views here.

def index(request):
    imoveis = Imovel.objects.all()
    frm_search_imoveis = SearchForm
    return render(request,"../templates/imovel/index.html",{'imoveis': imoveis,'frm_search_imoveis' : frm_search_imoveis})



def search_imoveis(request):
    
    kwargs = {} 

    if request.GET.get('endereco'):
        kwargs['endereco__contains'] = request.GET.get('endereco')

    if request.GET.get('cidade'):
        kwargs['cidade__contains'] = request.GET.get('cidade')
    
    if request.GET.get('bairro'):
        kwargs['bairro__contains'] = request.GET.get('bairro')
    
    if request.GET.get('estado'):
        kwargs['estado__uf'] = request.GET.get('estado')
    

    imoveis = Imovel.objects.filter(**kwargs) 
    frm_search_imoveis = SearchForm
    return render(request,"../templates/imovel/index.html",{'imoveis': imoveis,'frm_search_imoveis' : frm_search_imoveis})  


def cadastro(request):
    frm_imoveis = CadastroForm()
    return render(request,"../templates/imovel/cadastro.html",{'frm_imoveis':frm_imoveis})  

def editImovel(request,id):
    imovel = Imovel.objects.get(pk=id)
    frm_imoveis = CadastroForm(instance = imovel)
    return render(request,"../templates/imovel/cadastro.html",{'frm_imoveis':frm_imoveis, 'id':imovel.id})  



def saveImovel(request):
    if request.method == "POST":
        if request.POST.get('id'):
            imovel = Imovel.objects.get(pk=id)
            frm_imoveis = CadastroForm(instance = imovel)
        else:    
            frm_imoveis = CadastroForm(request.POST,request.FILES)
            if frm_imoveis.is_valid():
                frm_imoveis.save()
                messages.success(request,"Registro salvo com sucesso.") 

    return render(request,"../templates/imovel/cadastro.html",{'frm_imoveis':frm_imoveis})

def updateImovel(request):
    id = request.POST.get('id')
    imovel = get_object_or_404(Imovel, pk=id)
    if request.method == "POST":
        form = CadastroForm(request.POST,request.FILES,instance=imovel)
        if form.is_valid():
            form.save()
            messages.success(request,"Registro atualizado com sucesso.") 
            return render(request,"../templates/imovel/cadastro.html",{'frm_imoveis':form})
            #return redirect('post_detail', pk=post.pk)
    else:
        form = CadastroForm(instance=imovel)
    return render(request, "../templates/imovel/cadastro.html", {'frm_imoveis': form})


def deleteImovel(request):
    if request.method == "POST":
        id = request.POST.get("index")
        imv = Imovel.objects.get(pk=id)
        imv.delete()
        messages.success(request,"Registro deletado com sucesso.")

    imoveis = Imovel.objects.all()
    frm_search_imoveis = SearchForm
    return render(request,"../templates/imovel/index.html",{'imoveis': imoveis,'frm_search_imoveis' : frm_search_imoveis})





