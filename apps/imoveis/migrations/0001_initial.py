# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-10 18:43
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Aluguel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_vencimento', models.DateField()),
                ('aluguel', models.DecimalField(decimal_places=2, max_digits=5)),
                ('pago', models.BooleanField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Estado',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('estado', models.CharField(max_length=30)),
                ('uf', models.CharField(max_length=2)),
            ],
        ),
        migrations.CreateModel(
            name='Imovel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.CharField(max_length=120)),
                ('endereco', models.CharField(max_length=200)),
                ('bairro', models.CharField(max_length=80)),
                ('numero', models.CharField(max_length=6)),
                ('cidade', models.CharField(max_length=80)),
                ('valor_aluguel', models.DecimalField(decimal_places=2, max_digits=6)),
                ('alugado', models.BooleanField(default=0)),
                ('imagem', models.ImageField(upload_to='imoveis')),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Estado')),
            ],
        ),
        migrations.CreateModel(
            name='Locacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_locacao', models.DateField()),
                ('data_termino', models.DateField()),
                ('imovel', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Imovel')),
            ],
        ),
        migrations.CreateModel(
            name='Locador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cpf', models.CharField(max_length=14, unique=True)),
                ('telefone', models.CharField(max_length=11)),
                ('celular1', models.CharField(max_length=11)),
                ('celular2', models.CharField(max_length=11)),
                ('email', models.CharField(max_length=120)),
                ('endereco', models.CharField(max_length=200)),
                ('bairro', models.CharField(max_length=80)),
                ('numero', models.CharField(max_length=6)),
                ('cidade', models.CharField(max_length=80)),
                ('estado', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Estado')),
            ],
        ),
        migrations.CreateModel(
            name='Locatario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('cpf', models.CharField(max_length=14, unique=True)),
                ('telefone', models.CharField(max_length=11)),
                ('celular1', models.CharField(max_length=11)),
                ('celular2', models.CharField(max_length=11)),
                ('email', models.CharField(max_length=120)),
            ],
        ),
        migrations.AddField(
            model_name='locacao',
            name='locatario',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Locatario'),
        ),
        migrations.AddField(
            model_name='imovel',
            name='locador',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Locador'),
        ),
        migrations.AddField(
            model_name='aluguel',
            name='locacao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='imoveis.Locacao'),
        ),
    ]
