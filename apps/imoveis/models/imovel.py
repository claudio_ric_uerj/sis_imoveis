# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from locador import Locador
from estado import Estado

class Imovel(models.Model):
    locador = models.ForeignKey(Locador)
    email = models.CharField(max_length=120)
    endereco = models.CharField(max_length=200)
    bairro = models.CharField(max_length=80)
    numero = models.CharField(max_length=6)
    cidade = models.CharField(max_length=80)        
    estado = models.ForeignKey(Estado)
    valor_aluguel = models.DecimalField(max_digits=6,decimal_places=2)
    alugado = models.BooleanField(default=0)
    imagem = models.ImageField(upload_to='static/image/imoveis')

    class Meta:
        app_label = 'imoveis'