# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible
from django.db import models


@python_2_unicode_compatible
class Estado(models.Model):
    estado = models.CharField(max_length=30)
    uf = models.CharField(max_length=2)
    
    def __str__(self):
        return self.estado

    class Meta:
        app_label = 'imoveis'
