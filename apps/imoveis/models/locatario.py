# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Locatario(models.Model):
    nome = models.CharField(max_length=100)
    cpf  = models.CharField(max_length=14,unique=True)
    telefone = models.CharField(max_length=11)
    celular1 = models.CharField(max_length=11)
    celular2 = models.CharField(max_length=11)
    email = models.CharField(max_length=120)
    
    class Meta:
        app_label = 'imoveis'
    
    