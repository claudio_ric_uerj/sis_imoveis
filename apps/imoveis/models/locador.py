# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from estado import Estado

class Locador(models.Model):
    nome = models.CharField(max_length=100)
    cpf  = models.CharField(max_length=14,unique=True)
    telefone = models.CharField(max_length=11)
    celular1 = models.CharField(max_length=11)
    celular2 = models.CharField(max_length=11)
    email = models.CharField(max_length=120)
    endereco = models.CharField(max_length=200)
    bairro = models.CharField(max_length=80)
    numero = models.CharField(max_length=6)
    cidade = models.CharField(max_length=80)        
    estado = models.ForeignKey(Estado)
    #
    def __str__(self):
            return self.nome
            
    class Meta:
        app_label = 'imoveis'

             
