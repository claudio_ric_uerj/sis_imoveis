# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from locatario import Locatario
from locador import Locador
from imovel import Imovel

class Locacao(models.Model):
    imovel = models.ForeignKey(Imovel)
    locatario = models.ForeignKey(Locatario)
    data_locacao = models.DateField()
    data_termino = models.DateField()
    
    class Meta:
        app_label = 'imoveis'