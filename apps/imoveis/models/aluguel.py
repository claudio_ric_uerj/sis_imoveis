# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from locacao import Locacao

class Aluguel(models.Model):
    locacao = models.ForeignKey(Locacao)
    data_vencimento = models.DateField()
    aluguel = models.DecimalField(max_digits=5,decimal_places=2)  
    pago = models.BooleanField(default=0)

    class Meta:
        app_label = 'imoveis'