# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
#from apps.endereco_contato.models.endereco import *
#from apps.endereco_contato.models.contato import *
from models.aluguel import Aluguel
from models.imovel import Imovel 
from models.locacao import Locacao 
from models.locador import Locador 
from models.locatario import Locatario


# class EnderecoInline(admin.TabularInline):
#     model = Endereco
#     fk_name = 'to_locador'

class AluguelAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
   
class ImovelAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
       

class LocacaoAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
       

class LocadorAdmin(admin.ModelAdmin):
   def has_add_permission(self, request):
        return False
       

class LocatarioAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False
       


admin.site.register(Aluguel,AluguelAdmin)
admin.site.register(Imovel,ImovelAdmin)
admin.site.register(Locacao,LocacaoAdmin)
admin.site.register(Locador,LocadorAdmin)
admin.site.register(Locatario,LocatarioAdmin)


