from django import forms
from apps.imoveis.models import Imovel


class SearchForm(forms.Form):
    endereco = forms.CharField(label="Endereco",max_length=30,required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    cidade = forms.CharField(label="Cidade",max_length=30,required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    bairro = forms.CharField(label="Bairro",max_length=20,required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))
    estado = forms.CharField(label="Estado",max_length=2,required=False,widget=forms.TextInput(attrs={'class': 'form-control'}))

    



class CadastroForm(forms.ModelForm):
    
     class Meta:
         model = Imovel
         fields = '__all__'
         #('locador','email','endereco','bairro','cidade','estado','valor_aluguel','alugado','imagem')
         widgets = {
            'locador': forms.Select(attrs={'class': 'form-control','style':'width:60%'}),
            'email': forms.TextInput(attrs={'class': 'form-control','style':'width:100%'}),
            'endereco': forms.TextInput(attrs={'class': 'form-control','style':'width:100%'}),
            'numero': forms.TextInput(attrs={'class': 'form-control','style':'width:100%'}),
            'bairro': forms.TextInput(attrs={'class': 'form-control','style':'width:100%'}),
            'cidade': forms.TextInput(attrs={'class': 'form-control','style':'width:100%'}),
            'estado': forms.Select(attrs={'class': 'form-control','style':'width:100%'}),
            'valor_aluguel': forms.NumberInput(attrs={'class': 'form-control','style':'width:100%'}),
            'alugado': forms.CheckboxInput(attrs={'class': 'form-control'})            
         }   