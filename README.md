# README #



### Instalação do ambiente / migrates / loaddata /  ###

* O ambiente do sistema foi criado com virtualenv para desenvolvimento.

* O arquivo requirements.txt lista as baterias utilizados no sistema. Para instalar basta utilizar o comando pip install -r requirements.txt

* Após o uso de migrates para criação do banco de dados, pode-se rodar as fixtures para alimentar algumas tabela de teste
* python manage.py loaddata estados.json
* python manage.py loaddata locador.json


