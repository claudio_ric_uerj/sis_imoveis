"""sis_imoveis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from apps.imoveis import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^imoveis/edit/(?P<id>\d+)/$', views.editImovel, name='imoveis_edit'),
    url(r'^imoveis/del', views.deleteImovel, name='imoveis_delete'),
    url(r'^imoveis/update', views.updateImovel, name='imoveis_update'),
    url(r'^imoveis/save', views.saveImovel, name='imoveis_save'),
    url(r'^search/', views.search_imoveis, name='imoveis_search'),
    url(r'^imoveis/cadastro', views.cadastro, name='imoveis_cadastro'),
    url(r'^', views.index,name='index'),
    
]
